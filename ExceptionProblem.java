import java.util.Scanner;

public class ExceptionProblem {

    public static void checkThrows() throws UserException {
        throw new UserException();
    }

    public static void calculateExtraAmount(double luggageWeight) {

        double cost = 0.0;
        if (luggageWeight >= 15.1) {
            double extraWeight = (luggageWeight - 15);
            System.out.println(extraWeight + " kg");
            System.out.println();

            System.out.println("Additional cost to be paid = ");
            cost = extraWeight * 500;
        }
        System.out.println(cost);
    }

    public static void main(String[] args) {
        System.out.println("Enter the luggage weight = ");
        Scanner input = new Scanner(System.in);

        double luggageWeight = input.nextDouble();

        try {
            if (luggageWeight <= 15.1) {
                System.out.println("The weight is too small, it can be ignored");
            } else {
                checkThrows();
            }
        } catch (UserException e) {
            System.out.println(e.getMessage());
            System.out.println();

            System.out.println("Reduce luggage");
        } finally {
            calculateExtraAmount(luggageWeight);


        }

    }

    public static class UserException extends Exception {

        public UserException() {
            super("Luggage is more than 15kg");
        }
    }
}
